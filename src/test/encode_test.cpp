#include <engine/log.hpp>
#include <engine/network.hpp>

void test(uint32_t i) {
    uint8_t bytes[4];
    encode_u32(i, bytes);
    expect(i == decode_u32(bytes), "Test failed: ", i, " != ", decode_u32(bytes));
}

int main() {
    test(0);
    test(1);
    test(UINT32_MAX);
    test(42);
}