#include <shared_mutex>
#include <thread>

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>

#include <boost/asio/io_context.hpp>

#include <engine/log.hpp>
#include <engine/network.hpp>
#include <utility>

using namespace std::chrono_literals;

template<typename T>
class ThreadSafe {
public:
    template<typename... Args>
    ThreadSafe(Args &&...args) : object{std::make_unique<T>(std::forward<Args>(args)...)} {}

    explicit ThreadSafe(T *t) : object{std::unique_ptr<T>(t)} {}

    ThreadSafe() = delete;

    ThreadSafe(ThreadSafe const &) = delete;
    ThreadSafe(ThreadSafe &&) = default;

    ThreadSafe &operator=(ThreadSafe const &) = delete;
    ThreadSafe &operator=(ThreadSafe &&) = default;

    void set(T &&t) {
        std::unique_lock g{m};
        expect(object != nullptr, "Object is null");
        *object = std::move(t);
    }

    void set(const T &t) {
        std::unique_lock g{m};
        expect(object != nullptr, "Object is null");
        *object = t;
    }

    void write(std::function<void(T &)> const &f) {
        std::unique_lock g{m};
        expect(object != nullptr, "Object is null");
        f(*object);
    }

    void read(std::function<void(const T &)> const &f) const {
        std::shared_lock g{m};
        expect(object != nullptr, "Object is null");
        f(*object);
    }

private:
    mutable std::shared_mutex m;
    std::unique_ptr<T> object;
};

struct LocalGameState {
    // can be null before welcome message from server received
    std::optional<uint32_t> local_player_id;
    std::optional<uint32_t> local_actor_id;
};

class Client {
public:
    Client(boost::asio::io_context &context) : stream{context} {
        encoder.register_message_type<TextMessage>();
        encoder.register_message_type<MoveCommandMessage>(MoveActorCommand{UINT32_MAX, Position{}});
        encoder.register_message_type<GameStateDiffMessage>(GameStateDiff{UINT64_MAX});
        encoder.register_message_type<PlayerWelcomeMessage>(UINT32_MAX, UINT32_MAX);
    }

    bool connect(boost::asio::ip::tcp::endpoint const &endpoint) {
        int attempts = 3;
        while (attempts > 0) {
            bool res = stream.connect(endpoint);
            if (!res) {
                attempts--;
                std::this_thread::sleep_for(1s);
            } else {
                break;
            }
        }
        if (attempts == 0) {
            log("Failed to connect in 3 attempts");
            return false;
        }
        return true;
    }

    void send_message(Message const &msg) {
        Packet raw_msg;
        OutputStream s;
        encoder.encode(msg, s);
        raw_msg.append(s.data());
        stream.blocking_send(raw_msg);
    }

    void start_update() {
        stream.async_receive([this](auto raw_msg, auto ec) {
            if (ec) {
                return;
            }
            InputStream s{raw_msg.value().payload()};
            auto msg = encoder.decode(s);
            on_update(*msg);
            start_update();
        });
    }

    void set_on_update(std::function<void(Message const &)> on_update_) {
        on_update = std::move(on_update_);
    }

private:
    PacketStream stream;
    MessageEncoder encoder;
    std::function<void(Message const &)> on_update;
};

class MessageQueue {
public:
    MessageQueue() = default;

    MessageQueue(const MessageQueue &) = delete;
    MessageQueue(MessageQueue &&) = delete;

    MessageQueue &operator=(const MessageQueue &) = delete;
    MessageQueue &operator=(MessageQueue &&) = delete;

    void push(std::unique_ptr<Message> message) {
        expect(message != nullptr, "Null message");
        std::lock_guard g{mutex};
        queue.emplace_back(std::move(message));
    }

    std::optional<std::unique_ptr<Message>> pop() {
        std::lock_guard g{mutex};
        if (!queue.empty()) {
            auto msg = std::move(queue.back());
            queue.pop_back();
            return msg;
        }
        return std::nullopt;
    }

private:
    std::vector<std::unique_ptr<Message>> queue;
    std::mutex mutex{};
};

struct ClientMessageVisitor : public MessageVisitor {
public:
    ClientMessageVisitor(std::shared_ptr<GameState> game_state,
                         std::shared_ptr<ThreadSafe<LocalGameState>> local_game_state) : game_state{std::move(game_state)}, local_game_state{std::move(local_game_state)} {}

    void visit(GameStateDiffMessage const &m) override {
        auto &diff = m.get_diff();
        diff.apply(*game_state);
    }

    void visit(PlayerWelcomeMessage const &m) override {
        log("Welcome message: player #", m.get_self_player_id(), " actor #", m.get_owned_actor_id());
        local_game_state->write([&m](auto &state) {
            state.local_player_id = m.get_self_player_id();
            state.local_actor_id = m.get_owned_actor_id();
        });
    }

private:
    std::shared_ptr<GameState> game_state;
    std::shared_ptr<ThreadSafe<LocalGameState>> local_game_state;
};

int main() {
    static std::atomic_flag exit_flag;
    auto message_queue = std::make_shared<MessageQueue>();
    auto game_state = std::make_shared<GameState>(GameState::OnClientTag{});
    auto local_game_state = std::make_shared<ThreadSafe<LocalGameState>>(LocalGameState{});

    auto network_thread = std::thread([](std::shared_ptr<MessageQueue> message_queue, std::shared_ptr<GameState> game_state, std::shared_ptr<ThreadSafe<LocalGameState>> local_game_state) {
        boost::asio::io_context context;

        boost::asio::ip::tcp::endpoint endpoint(
                boost::asio::ip::make_address_v4("127.0.0.1"), 54321);
        Client client{context};
        expect(client.connect(endpoint), "Client connection failed");

        auto message_visitor = std::make_unique<ClientMessageVisitor>(game_state, local_game_state);

        client.set_on_update([&message_visitor](auto &msg) {
            msg.visit(*message_visitor);
        });
        client.start_update();

        struct MessageSender {
            void send() {
                if (auto msg_opt = queue->pop()) {
                    client.send_message(*msg_opt.value());
                }
                context.post([this]() { send(); });
            };

            boost::asio::io_context &context;
            std::shared_ptr<MessageQueue> queue;
            Client &client;
        };
        MessageSender sender{context, message_queue, client};
        sender.send();

        while (!exit_flag.test()) {
            context.run_one();
        }
    },
                                      message_queue, game_state, local_game_state);
    sf::RenderWindow window{sf::VideoMode{800, 600}, "Bugs"};

    std::vector<sf::RectangleShape> actor_shapes;
    while (window.isOpen()) {
        sf::Event event{};
        while (window.pollEvent(event)) {
            switch (event.type) {
                case sf::Event::EventType::Closed:
                    window.close();
                    break;
                case sf::Event::KeyPressed: {
                    int dx = 0;
                    int dy = 0;
                    switch (event.key.code) {
                        case sf::Keyboard::Up:
                            dy -= 10;
                            break;
                        case sf::Keyboard::Down:
                            dy += 10;
                            break;
                        case sf::Keyboard::Right:
                            dx += 10;
                            break;
                        case sf::Keyboard::Left:
                            dx -= 10;
                            break;
                        default:
                            break;
                    }
                    std::optional<uint32_t> local_actor_id;
                    local_game_state->read([&local_actor_id](auto &state) {
                        local_actor_id = state.local_actor_id;
                    });
                    if (local_actor_id) {
                        message_queue->push(std::make_unique<MoveCommandMessage>(MoveActorCommand{local_actor_id.value(), {dx, dy}}));
                    }
                } break;
                default:
                    break;
            }
        }
        game_state->for_all_actors([&actor_shapes](auto idx, auto &actor) {
            if (actor_shapes.size() <= idx) {
                actor_shapes.resize(idx + 1);
                actor_shapes[idx].setSize({100, 100});
            }
            actor_shapes[idx].setPosition(actor.pos.x, actor.pos.y);
        });

        window.clear(sf::Color::Cyan);
        for (auto &actor: actor_shapes) window.draw(actor);
        window.display();
    }

    exit_flag.test_and_set();

    network_thread.join();
}
