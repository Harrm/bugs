#pragma once

#include <list>
#include <memory>
#include <optional>
#include <span>
#include <unordered_map>

#include <boost/algorithm/hex.hpp>
#include <boost/asio/ip/tcp.hpp>

#include <engine/message.hpp>

struct Packet {
public:
    Packet(): data{0, 0, 0, 0} {}

    std::span<const uint8_t> payload() const {
        return std::span{data}.subspan(4);
    }

    std::span<uint8_t> payload() {
        return std::span{data}.subspan(4);
    }

    void append(std::span<const uint8_t> new_data) {
        data.resize(data.size() + new_data.size());
        encode_u32(data.size() - 4, data.data());
        std::copy_n(new_data.begin(), new_data.size(), data.end() - new_data.size());
    }

    void resize(size_t size) {
        data.resize(data.size() + size);
        encode_u32(data.size() - 4, data.data());
    }

    std::span<const uint8_t> header() const {
        return std::span{data}.subspan(0, 4);
    }

    std::span<const uint8_t> header_and_payload() const {
        return data;
    }

private:
    std::vector<uint8_t> data;
};

using SendCallback = std::function<void(boost::system::error_code)>;
template<typename Payload>
using ReceiveCallback = std::function<void(std::optional<Payload>, boost::system::error_code)>;

template<typename Payload>
class Stream {
    virtual boost::system::error_code blocking_send(Payload const& packet) = 0;

    virtual void async_send(Payload packet, SendCallback && cb) = 0;

    virtual void async_receive(ReceiveCallback<Payload>&& callback) = 0;

};

class PacketStream final: public Stream<Packet> {
public:
    explicit PacketStream(boost::asio::io_context &context)
        : socket{context} {}

    bool connect(boost::asio::ip::tcp::endpoint const &endpoint) {
        socket.open(boost::asio::ip::tcp::v4());
        boost::system::error_code ec;
        socket.connect(endpoint, ec);
        if (ec) {
            log("Failed to connect: ", ec.message());
            socket.close();
            return false;
        }
        return true;
    }

    boost::system::error_code blocking_send(Packet const& packet) override {
        auto remaining_bytes = std::span{packet.header_and_payload()};
        std::string s(remaining_bytes.size() * 2, ' ');
        boost::algorithm::hex(remaining_bytes.begin(), remaining_bytes.end(), s.begin());
        size_t total_bytes_written = 0;
        boost::system::error_code ec;
        while (total_bytes_written != packet.header_and_payload().size()) {
            auto bytes_written =
                    socket.write_some(boost::asio::buffer(remaining_bytes), ec);
            if (ec) {
                return ec;
            }
            remaining_bytes = remaining_bytes.subspan(bytes_written);
            total_bytes_written += bytes_written;
        }
        return {};
    }

    using SendCallback = std::function<void(boost::system::error_code)>;
    void async_send(Packet packet, SendCallback && cb) override {
        std::string s(packet.header_and_payload().size() * 2, ' ');
        boost::algorithm::hex(packet.header_and_payload().begin(), packet.header_and_payload().end(), s.begin());

        socket.async_write_some(boost::asio::const_buffer(packet.header_and_payload().data(), packet.header_and_payload().size()),
                                [this, cb=std::move(cb), packet = std::move(packet)](auto ec, auto bytes_num) mutable {
                                    if (ec) {
                                        log("Failed to send message to client: ", ec.message());
                                        cb(ec);
                                        return;
                                    }
                                    if (bytes_num < 4) {
                                        log("Failed to send message to client.");
                                        cb(ec);
                                        return;
                                    }
                                    auto msg = std::make_unique<InProgressMessage>(InProgressMessage{std::move(packet), bytes_num - 4});
                                    continue_send(std::move(msg), std::move(cb));
                                });
    }

    using ReceiveCallback = std::function<void(std::optional<Packet>, boost::system::error_code)>;
    void async_receive(ReceiveCallback&& callback) override {
        auto msg_size_bytes = std::make_unique<std::array<uint8_t, 4>>();
        socket.async_read_some(
                boost::asio::buffer(*msg_size_bytes, 4),
                [this, callback = std::move(callback), msg_size_bytes = std::move(msg_size_bytes)](auto ec, auto bytes_read) mutable {
                    if (ec) {
                        log("Failed to receive a message: ", ec.message());
                        callback({}, ec);
                        return;
                    }
                    expect(bytes_read >= 4, "Failed to read msg size");

                    std::string s(8, ' ');
                    boost::algorithm::hex(msg_size_bytes->begin(), msg_size_bytes->end(), s.begin());

                    Packet packet;
                    auto size = decode_u32(msg_size_bytes->data());
                    packet.resize(size);
                    auto msg = std::make_unique<InProgressMessage>(InProgressMessage{std::move(packet), 0});
                    continue_read(std::move(msg), callback);
                });
    }

    boost::asio::ip::tcp::socket &remote() {
        return socket;
    }

private:
    struct InProgressMessage {
        Packet packet;
        size_t processed_bytes = 0;

        std::span<const uint8_t> in_progress_data() const {
            return std::span(packet.payload().data() + processed_bytes,
                             packet.payload().data() + packet.payload().size());
        }

        std::span<uint8_t> in_progress_data() {
            return std::span(packet.payload().data() + processed_bytes,
                             packet.payload().data() + packet.payload().size());
        }
    };

    void continue_send(std::unique_ptr<InProgressMessage>&& msg, SendCallback&& cb) {
        std::string s(msg->in_progress_data().size() * 2, ' ');
        boost::algorithm::hex(msg->in_progress_data().begin(), msg->in_progress_data().end(), s.begin());

        socket.async_write_some(
                boost::asio::const_buffer(msg->in_progress_data().data(),
                                          msg->in_progress_data().size()),
                [this, cb=std::move(cb), msg = std::move(msg)](auto ec, auto bytes_num)mutable  {
                    if (ec) {
                        log("Error: ", ec.message());
                        cb(ec);
                        return;
                    }
                    msg->processed_bytes += bytes_num;
                    if (msg->processed_bytes < msg->packet.payload().size()) {
                        continue_send(std::move(msg), std::move(cb));
                    }
                    cb(boost::system::error_code{});
                });
    }

    void continue_read(std::unique_ptr<InProgressMessage>&& msg, ReceiveCallback const& callback) {
        socket.async_read_some(boost::asio::buffer(msg->in_progress_data().data(),
                                                   msg->in_progress_data().size()),
                               [this, msg=std::move(msg), callback=std::move(callback)](auto ec, auto bytes_num) mutable {
                                   if (ec) {
                                       log("Error: ", ec.message());
                                       callback({}, ec);
                                       return;
                                   }
                                   std::string s(bytes_num * 2, ' ');
                                   boost::algorithm::hex(msg->in_progress_data().begin(), msg->in_progress_data().begin() + bytes_num, s.begin());
                                   msg->processed_bytes += bytes_num;

                                   if (msg->processed_bytes < msg->packet.payload().size()) {
                                       continue_read(std::move(msg), callback);
                                   } else {
                                       callback(msg->packet, {});
                                   }
                               });
    }

    boost::asio::ip::tcp::socket socket;
};

class MessageEncoder {
public:
    template<typename T, typename... Args>
    bool register_message_type(Args &&...args) {
        return register_message_type(std::make_unique<T>(std::forward<Args>(args)...));
    }

    bool register_message_type(std::unique_ptr<Message> &&message_template) {
        if (message_templates.contains(message_template->get_type_id())) {
            return false;
        }
        message_templates.emplace(message_template->get_type_id(), std::move(message_template));
        return true;
    }

    std::unique_ptr<Message> decode(InputStream& stream) const {
        uint32_t type;
        stream >> type;

        auto msg = message_templates.at(type)->clone();
        stream >> *msg;
        return msg;
    }

    void encode(Message const &message, OutputStream& stream) {
        stream << message.get_type_id() << message;
    }

private:
    std::unordered_map<uint32_t, std::unique_ptr<Message>> message_templates;
};
