//
// Created by Vladimir on 16.11.2022.
//

#pragma once

#include <span>

#include <engine/log.hpp>

uint32_t decode_u32(uint8_t* bytes) {
    uint32_t res = bytes[3];
    res <<= 8;
    res |= bytes[2];
    res <<= 8;
    res |= bytes[1];
    res <<= 8;
    res |= bytes[0];
    return res;
}

int32_t decode_i32(uint8_t* bytes) {
    int32_t n;
    std::copy_n(bytes, 4, reinterpret_cast<uint8_t*>(&n));
    return n;
}

void encode_u32(uint32_t n, uint8_t *dest) {
    dest[0] = n & 0xFF;
    dest[1] = (n >> 8) & 0xFF;
    dest[2] = (n >> 16) & 0xFF;
    dest[3] = (n >> 24) & 0xFF;
}

void encode_i32(int32_t n, uint8_t* dest) {
    std::copy_n(reinterpret_cast<uint8_t*>(&n), 4, dest);
}

class Serializable {
public:
    virtual void decode(class InputStream& stream) = 0;
    virtual void encode(class OutputStream& stream) const = 0;
    virtual size_t size_hint() const = 0;
};

class InputStream {
public:
    InputStream(std::span<uint8_t> bytes): bytes{bytes}{}

    InputStream& operator>>(uint32_t& n) {
        expect(bytes.size() >= 4, "Not enough data to decode");
        n = decode_u32(bytes.data());
        bytes = bytes.subspan(4);
        return *this;
    }

    InputStream& operator>>(uint64_t& n) {
        expect(bytes.size() >= 4, "Not enough data to decode");
        n = decode_u32(bytes.data());
        n |= static_cast<uint64_t>(decode_u32(bytes.subspan(4).data())) << 32;
        bytes = bytes.subspan(8);
        return *this;
    }

    InputStream& operator>>(int32_t& n) {
        expect(bytes.size() >= 4, "Not enough data to decode");
        n = decode_i32(bytes.data());
        bytes = bytes.subspan(4);
        return *this;
    }

    InputStream& operator>>(uint8_t& byte) {
        expect(bytes.size() >= 1, "Not enough data to decode");
        byte = bytes[0];
        bytes = bytes.subspan(1);
        return *this;
    }

    InputStream& operator>>(char& byte) {
        expect(bytes.size() >= 1, "Not enough data to decode");
        byte = bytes[0];
        bytes = bytes.subspan(1);
        return *this;
    }

    template<typename T>
    InputStream& operator>>(std::vector<T>& v) {
        uint32_t size;
        *this >> size;
        v.resize(size);
        for(auto& elem: v) {
            *this >> elem;
        }
        return *this;
    }

    InputStream& operator>>(std::string& s) {
        uint32_t size;
        *this >> size;
        s.resize(size);
        for(auto& elem: s) {
            *this >> elem;
        }
        return *this;
    }

    InputStream& operator>>(Serializable& ser) {
        expect(bytes.size() >= ser.size_hint(), "Not enough data to decode");
        ser.decode(*this);
        bytes = bytes.subspan(ser.size_hint());
        return *this;
    }

private:
    std::span<uint8_t> bytes;
};

class OutputStream {
public:
    OutputStream& operator<<(uint32_t n) {
        bytes.resize(bytes.size() + 4);
        encode_u32(n, bytes.data() + bytes.size() - 4);
        return *this;
    }

    OutputStream& operator<<(uint64_t n) {
        bytes.resize(bytes.size() + 8);
        uint32_t high = n >> 32;
        uint32_t low = n & 0xFFFFFFFF;
        encode_u32(low, bytes.data() + bytes.size() - 8);
        encode_u32(high, bytes.data() + bytes.size() - 4);
        return *this;
    }

    OutputStream& operator<<(int32_t n) {
        bytes.resize(bytes.size() + 4);
        encode_u32(n, bytes.data() + bytes.size() - 4);
        return *this;
    }

    OutputStream& operator<<(Serializable const& ser) {
        //bytes.resize(bytes.size() + ser.size_hint());
        ser.encode(*this);
        return *this;
    }


    OutputStream& operator<<(uint8_t byte) {
        bytes.resize(bytes.size() + 1);
        bytes.back() = byte;
        return *this;
    }

    template<typename T>
    OutputStream& operator<<(std::vector<T> const& v) {
        uint32_t size = v.size();
        *this << size;
        for(auto& elem: v) {
            *this << elem;
        }
        return *this;
    }

    OutputStream& operator<<(std::string_view s) {
        uint32_t size = s.size();
        *this << size;
        for(auto& elem: s) {
            *this << elem;
        }
        return *this;
    }

    std::vector<uint8_t> const& data() const {
        return bytes;
    }

    std::vector<uint8_t>& data() {
        return bytes;
    }

private:
    std::vector<uint8_t> bytes;
};

template<typename... Objects>
std::vector<uint8_t> serialize(Objects const&... objects) {
    OutputStream stream;
    auto tuple = std::make_tuple<Objects const&...>(objects...);
    for (auto& object: tuple) {
        stream << object;
    }
    return std::move(stream.data());
}

template<typename Object>
void deserialize(std::span<uint8_t> bytes, Object& obj) {
    InputStream stream{bytes};
    stream >> obj;
}
