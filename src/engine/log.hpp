#pragma once

#include <iostream>

#include <boost/system/error_code.hpp>

template <typename... T> void panic_(const T &...args) {
  static_cast<void>((std::cerr << ... << args));
  std::exit(-1);
}

#define panic(...) panic_(__FILE__, ":", __LINE__, ": ", __VA_ARGS__)

template <typename... T> void log(const T &...args) {
  (std::cout << ... << args) << "\n";
}

void unwrap_(std::string_view file, int line, boost::system::error_code const &ec) {
  if (ec) {
    panic_(file, ":", line, ": Error code: ", ec.message());
  }
}

#define unwrap(ec) unwrap_(__FILE__, __LINE__, ec)

template <typename... T> void expect_(bool condition, const T &...args) {
  if (!condition) {
    panic(args...);
  }
}

#define expect(cond, ...) expect_((cond), __FILE__, ":", __LINE__, ": ", __VA_ARGS__)
