//
// Created by Vladimir on 15.11.2022.
//

#pragma once

#include <optional>
#include <vector>

#include <engine/serializable.hpp>

struct Position {
    int32_t x;
    int32_t y;
};

std::ostream &operator<<(std::ostream &s, Position const &pos) {
    return s << "(" << pos.x << ", " << pos.y << ")";
}

struct Actor {
    uint32_t id;
    Position pos;
};

struct Player {
    std::string name;
    uint32_t id;
};

template<typename T>
class Diff {
public:
    virtual ~Diff() = default;

    virtual void apply(T &t) const = 0;
};

class ActorDiff : Diff<Actor> {
public:
    void apply(Actor &a) const override {
        a.pos.x += pos_diff.x;
        a.pos.y += pos_diff.y;
    }

    Position pos_diff{};
};

struct GameStateDiff : public Diff<class GameState>, public Serializable {
    explicit GameStateDiff(uint64_t sequence_number) : sequence_number{sequence_number} {}

    GameStateDiff(const GameStateDiff &) = default;
    GameStateDiff &operator=(const GameStateDiff &) = delete;

    GameStateDiff(GameStateDiff &&) = default;
    GameStateDiff &operator=(GameStateDiff &&) = delete;

    void apply(GameState &target_state) const override;

    void decode(InputStream &s) override;
    void encode(OutputStream &s) const override;
    size_t size_hint() const override;

    const uint64_t sequence_number;
    std::vector<Actor> new_actors;
    std::vector<Player> new_players;
    std::vector<std::pair<uint32_t, ActorDiff>> actor_diffs;
};

class GameStateDiffRecorder {
public:
    static std::unique_ptr<GameStateDiffRecorder> start(GameState const &base_state) {
        return std::unique_ptr<GameStateDiffRecorder>(new GameStateDiffRecorder{base_state});
    }

    GameStateDiff record() && {
        return diff;
    }

    uint32_t add_actor(Position pos);

    uint32_t add_player(std::string const &name);

    void modify_actor(uint32_t idx, ActorDiff const &actor_diff) {
        diff.actor_diffs.emplace_back(idx, actor_diff);
    }

private:
    explicit GameStateDiffRecorder(GameState const &base_state);

    GameState const &base_state;
    GameStateDiff diff;
};

class Command : public Serializable {
public:
    virtual void apply(GameStateDiffRecorder &state_diff) const = 0;
};

class GameState {

    friend struct GameStateDiff;

public:
    struct OnServerTag {};
    struct OnClientTag {};

    explicit GameState(OnServerTag const &) : sequence_number{0} {
    }

    explicit GameState(OnClientTag const &) : sequence_number{UINT64_MAX} {
    }

    template<typename Func>
    void for_actor(size_t idx, Func &&func) const {
        std::lock_guard g{mutex};
        if (idx < actors.size()) {
            func(std::make_optional(std::cref(actors.at(idx))));
        } else {
            func(std::nullopt);
        }
    }

    template<typename Func>
    void for_all_actors(Func &&func) const {
        std::lock_guard g{mutex};
        uint32_t idx = 0;
        for (auto &actor: actors) {
            func(idx, actor);
            idx++;
        }
    }

    GameStateDiff to_diff() const {
        std::lock_guard g{mutex};
        GameStateDiff diff{sequence_number};
        for (auto &player: players) {
            diff.new_players.emplace_back(player);
        }
        for (auto &actor: actors) {
            diff.new_actors.emplace_back(actor);
        }
        return diff;
    }

    size_t get_players_num() const {
        std::lock_guard g{mutex};
        return players.size();
    }

    size_t get_actors_num() const {
        std::lock_guard g{mutex};
        return actors.size();
    }

    void apply(GameStateDiff const &diff);

    uint64_t get_sequence_number() const {
        return sequence_number;
    }

private:
    std::vector<Player> players;
    std::vector<Actor> actors;
    std::atomic<uint64_t> sequence_number;
    mutable std::mutex mutex;
};

GameStateDiffRecorder::GameStateDiffRecorder(GameState const &base_state) : base_state{base_state}, diff{base_state.get_sequence_number() + 1} {
}

uint32_t GameStateDiffRecorder::add_actor(Position pos) {
    uint32_t id = base_state.get_actors_num() + diff.new_actors.size();
    diff.new_actors.emplace_back(Actor{id, pos});
    return id;
}

uint32_t GameStateDiffRecorder::add_player(std::string const &name) {
    uint32_t id = base_state.get_players_num() + diff.new_players.size();
    diff.new_players.emplace_back(Player{name, id});
    return id;
}

void GameStateDiff::apply(GameState &state) const {
    state.apply(*this);
}

void GameState::apply(GameStateDiff const &diff) {
    const uint64_t current_seq_number = sequence_number;
    if (current_seq_number != UINT64_MAX && current_seq_number != diff.sequence_number - 1) {
        log("Cannot apply state diff: mismatching state sequence numbers (incoming)", diff.sequence_number, ", (current)", current_seq_number);
        return;
    }
    std::lock_guard g{mutex};
    for (auto &actor: diff.new_actors) {
        log("New actor #", actors.size(), " pos: ", actor.pos);
        actors.emplace_back(actor);
    }
    for (auto &[idx, actor_diff]: diff.actor_diffs) {
        if (actors.size() <= idx) {
            log("Actor #", idx, " doesn't exist");
        } else {
            actor_diff.apply(actors.at(idx));
            log("Actor #", idx, " pos changed by ", actor_diff.pos_diff, " to ", actors.at(idx).pos);
        }
    }
    if (current_seq_number == UINT64_MAX) {
        sequence_number = diff.sequence_number;
    } else {
        sequence_number++;
    }
}

void GameStateDiff::decode(InputStream &s) {
    s >> const_cast<uint64_t&>(sequence_number);
    uint32_t new_actors_size;
    s >> new_actors_size;
    new_actors.resize(new_actors_size);
    for (size_t i = 0; i < new_actors_size; i++) {
        s >> new_actors[i].pos.x;
        s >> new_actors[i].pos.y;
    }
    uint32_t actor_diffs_size;
    s >> actor_diffs_size;
    actor_diffs.resize(actor_diffs_size);
    for (size_t i = 0; i < actor_diffs_size; i++) {
        s >> actor_diffs[i].first;
        s >> actor_diffs[i].second.pos_diff.x;
        s >> actor_diffs[i].second.pos_diff.y;
    }
}

void GameStateDiff::encode(OutputStream &s) const {
    s << sequence_number;
    s << (uint32_t) new_actors.size();
    for (auto &actor: new_actors) {
        s << actor.pos.x;
        s << actor.pos.y;
    }
    s << (uint32_t) actor_diffs.size();
    for (auto &[id, diff]: actor_diffs) {
        s << id;
        s << diff.pos_diff.x;
        s << diff.pos_diff.y;
    }
}

size_t GameStateDiff::size_hint() const {
    return 4 + new_actors.size() * sizeof(Actor) + 4 + actor_diffs.size() * (4 + sizeof(ActorDiff));
}

class MoveActorCommand : public Command {
public:
    MoveActorCommand(uint32_t actor_id,
                     Position pos_diff) : actor_id{actor_id}, pos_diff{pos_diff} {}

    void apply(GameStateDiffRecorder &state_diff) const override {
        log("Move ", actor_id, " to ", pos_diff.x, ", ", pos_diff.y);
        ActorDiff diff;
        diff.pos_diff = pos_diff;
        state_diff.modify_actor(actor_id, diff);
    }

    void decode(InputStream &stream) override {
        stream >> actor_id >> pos_diff.x >> pos_diff.y;
    }

    void encode(OutputStream &stream) const override {
        stream << actor_id << pos_diff.x << pos_diff.y;
    }

    size_t size_hint() const override {
        return sizeof(actor_id) + sizeof(Position);
    }

private:
    uint32_t actor_id;
    Position pos_diff;
};
