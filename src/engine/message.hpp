//
// Created by Vladimir on 16.11.2022.
//

#pragma once

#include <span>

#include <engine/game.hpp>

uint32_t get_unique_id() {
    static std::atomic<uint32_t> id = 0;
    return id++;
}

template<typename T>
class Clone {
public:
    virtual std::unique_ptr<T> clone() const = 0;
};

class Message : public Clone<Message>, public Serializable {
public:
    virtual ~Message() = default;

    virtual uint32_t get_type_id() const = 0;

    virtual void visit(class MessageVisitor &visitor) const = 0;
};

class TextMessage final : public Message {
public:
    void decode(InputStream &stream) override {
        stream >> text;
    }

    void encode(OutputStream &stream) const override {
        stream << text;
    }

    size_t size_hint() const override {
        return text.size();
    }

    std::unique_ptr<Message> clone() const override {
        auto msg = std::make_unique<TextMessage>();
        return msg;
    }

    uint32_t get_type_id() const override {
        return type_id;
    }

    void visit(class MessageVisitor &visitor) const override;

    std::string text;
    const static inline uint32_t type_id = get_unique_id();
};

class MoveCommandMessage final : public Message {
public:
    explicit MoveCommandMessage(MoveActorCommand cmd) : cmd{std::move(cmd)} {}

    void decode(InputStream &stream) override {
        stream >> cmd;
    }
    void encode(OutputStream &stream) const override {
        stream << cmd;
    }
    size_t size_hint() const override {
        return cmd.size_hint();
    }
    std::unique_ptr<Message> clone() const override {
        return std::make_unique<MoveCommandMessage>(cmd);
    }

    void visit(class MessageVisitor &visitor) const override;

    MoveActorCommand const &get_command() const {
        return cmd;
    }

    uint32_t get_type_id() const override {
        return type_id;
    }

private:
    MoveActorCommand cmd;

    const static inline uint32_t type_id = get_unique_id();
};

class GameStateDiffMessage final : public Message {
public:
    explicit GameStateDiffMessage(const GameStateDiff &diff) : diff{diff} {}

    void decode(InputStream &stream) override {
        stream >> diff;
    }

    void encode(OutputStream &stream) const override {
        stream << diff;
    }

    size_t size_hint() const override {
        return diff.size_hint();
    }

    std::unique_ptr<Message> clone() const override {
        return std::make_unique<GameStateDiffMessage>(GameStateDiff{0});
    }

    void visit(class MessageVisitor &visitor) const override;

    uint32_t get_type_id() const override {
        return type_id;
    }

    GameStateDiff const &get_diff() const {
        return diff;
    }

private:
    GameStateDiff diff;
    const static inline uint32_t type_id = get_unique_id();
};

class PlayerWelcomeMessage final : public Message {
public:
    explicit PlayerWelcomeMessage(uint32_t player_id, uint32_t actor_id) : player_id{player_id}, actor_id{actor_id} {}

    std::unique_ptr<Message> clone() const override {
        return std::make_unique<PlayerWelcomeMessage>(UINT32_MAX, UINT32_MAX);
    }

    uint32_t get_type_id() const override {
        return type_id;
    }

    void visit(MessageVisitor &visitor) const override;

    void decode(InputStream &stream) override {
        stream >> player_id >> actor_id;
    }
    void encode(OutputStream &stream) const override {
        stream << player_id << actor_id;
    }
    size_t size_hint() const override {
        return sizeof(player_id) + sizeof(actor_id);
    }

    uint32_t get_self_player_id() const {
        return player_id;
    }

    uint32_t get_owned_actor_id() const {
        return actor_id;
    }

private:
    const static inline uint32_t type_id = get_unique_id();
    uint32_t player_id;
    uint32_t actor_id;
};

class MessageVisitor {
public:
    virtual ~MessageVisitor() = default;

    virtual void visit(MoveCommandMessage const &) {}
    virtual void visit(GameStateDiffMessage const &) {}
    virtual void visit(TextMessage const &) {}
    virtual void visit(PlayerWelcomeMessage const &) {}
    virtual void visit_other(Message const &) {}
};

void MoveCommandMessage::visit(class MessageVisitor &visitor) const {
    visitor.visit(*this);
}

void GameStateDiffMessage::visit(class MessageVisitor &visitor) const {
    visitor.visit(*this);
}

void TextMessage::visit(class MessageVisitor &visitor) const {
    visitor.visit(*this);
}

void PlayerWelcomeMessage::visit(class MessageVisitor &visitor) const {
    visitor.visit(*this);
}

class CommandMessageVisitor : public MessageVisitor {
public:
    explicit CommandMessageVisitor(GameStateDiffRecorder &diff_recorder) : diff_recorder{diff_recorder} {
    }

    void visit(MoveCommandMessage const &m) override {
        m.get_command().apply(diff_recorder);
    }

private:
    GameStateDiffRecorder &diff_recorder;
};