#include <list>
#include <random>
#include <thread>

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>

#include <engine/game.hpp>
#include <engine/log.hpp>
#include <engine/network.hpp>

using namespace std::chrono_literals;

Position random_position(Position min, Position max) {
    static std::mt19937_64 random_engine{clock()};
    std::uniform_int_distribution dist_x{min.x, max.x};
    std::uniform_int_distribution dist_y{min.y, max.y};
    return {dist_x(random_engine), dist_y(random_engine)};
}

class Server {
public:
    Server(boost::asio::io_context &context, const boost::asio::ip::tcp::endpoint &endpoint) : context{context},
                                                                                               acceptor{
                                                                                                       context,
                                                                                                       endpoint,
                                                                                               },
                                                                                               update_timer{context} {}

    void run() {
        encoder.register_message_type<TextMessage>();
        encoder.register_message_type<MoveCommandMessage>(MoveActorCommand{UINT32_MAX, Position{}});
        encoder.register_message_type<GameStateDiffMessage>(GameStateDiff{0});
        encoder.register_message_type<PlayerWelcomeMessage>(UINT32_MAX, UINT32_MAX);

        acceptor.listen();
        log("Listening");
        accept_stream();
        start_update();
    }

    void subscribe(std::function<void(Message const &)> f) {
        subscribers.emplace_back(std::move(f));
    }

    void on_update(std::function<void()> f) {
        update_subscribers.emplace_back(std::move(f));
    }

    void broadcast(Message const &message) {
        Packet raw_msg;
        OutputStream s{};
        encoder.encode(message, s);
        raw_msg.append(s.data());
        size_t idx = 0;
        for (auto &stream: streams) {
            stream->async_send(raw_msg, [this, idx](auto ec) {
                if (ec == boost::system::errc::broken_pipe) {
                    disconnected_streams.push_back(idx);
                }
            });
            idx++;
        }
    }

    void send(PacketStream &stream, Message const &message) {
        Packet raw_msg;
        OutputStream s;
        encoder.encode(message, s);
        raw_msg.append(s.data());
        stream.async_send(raw_msg, [](auto) {});
    }

    void set_on_connect(std::function<void(PacketStream&)> on_connect_) {
        on_connect = std::move(on_connect_);
    }

private:
    void accept_stream() {
        auto stream = std::make_shared<PacketStream>(context);
        acceptor.async_accept(stream->remote(), [this, stream](boost::system::error_code const &ec) mutable {
            unwrap(ec);
            log("Connected to ", stream->remote().remote_endpoint().address().to_string());
            streams.emplace_back(stream);
            on_connect(*stream);
            run_stream(*stream);
            accept_stream();
        });
    }

    void start_update() {
        update_timer.expires_after(16ms);
        update_timer.async_wait([this](auto ec) {
            unwrap(ec);
            size_t last_valid_stream = streams.size() - 1;
            for (auto stream_idx: disconnected_streams) {
                std::swap(streams[stream_idx], streams[last_valid_stream]);
                last_valid_stream--;
            }
            streams.erase(streams.begin() + last_valid_stream + 1, streams.end());
            disconnected_streams.clear();
            for (auto &subscriber: update_subscribers) {
                subscriber();
            }
            start_update();
        });
    }

    void run_stream(PacketStream &stream) {
        stream.async_receive([this, &stream](auto msg_data, auto ec) {
            if (ec) {
                return;
            }
            InputStream s{msg_data.value().payload()};
            auto msg = encoder.decode(s);
            for (auto &subscriber: subscribers) {
                subscriber(*msg);
            }
            run_stream(stream);
        });
    }

    boost::asio::io_context &context;
    boost::asio::ip::tcp::acceptor acceptor;
    std::vector<std::shared_ptr<PacketStream>> streams;
    std::vector<std::function<void(Message const &)>> subscribers;
    std::vector<std::function<void()>> update_subscribers;
    std::function<void(PacketStream &)> on_connect;
    boost::asio::steady_timer update_timer;
    std::vector<size_t> disconnected_streams;

    MessageEncoder encoder;
};

int main() {
    boost::asio::io_context context;

    boost::asio::ip::tcp::endpoint endpoint{
            boost::asio::ip::make_address_v4("127.0.0.1"), 54321};

    Server server{context, endpoint};

    GameState state{GameState::OnServerTag{}};

    auto diff_recorder = GameStateDiffRecorder::start(state);

    server.set_on_connect([&state, &server, &diff_recorder](auto &stream) {
        Position pos = random_position({0, 0}, {500, 500});

        auto player_id = diff_recorder->add_player("test");
        auto actor_id = diff_recorder->add_actor(pos);

        auto full_diff = state.to_diff();
        GameStateDiffMessage msg{full_diff};
        server.send(stream, msg);

        PlayerWelcomeMessage welcome_msg{player_id, actor_id};
        server.send(stream, welcome_msg);
    });

    server.subscribe([&diff_recorder](auto &msg) {
        CommandMessageVisitor command_msg_visitor{*diff_recorder};
        msg.visit(command_msg_visitor);
    });

    server.on_update([&server, &diff_recorder, &state]() {
        auto diff = std::move(*diff_recorder).record();
        diff.apply(state);
        diff_recorder = GameStateDiffRecorder::start(state);
        GameStateDiffMessage msg{diff};
        server.broadcast(msg);
    });

    context.dispatch([&server]() {
        server.run();
    });

    bool should_exit = false;

    while (!should_exit) {
        context.run();
    }

    return 0;
}
